#!/bin/sh

# assist: run this if your super + space instantmenu stops working

notify-send '[instantASSIST] applied instantmenu workaround'
rm ~/.cache/instantmenu_run
